module.exports = {
  pluginOptions: {
    vuetify: {
			// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
		}
  },
  pages: {
    index: {
      // entry for the page
      entry: 'src/main.js',
      title: 'NFTlanding',
    },
  },
}
