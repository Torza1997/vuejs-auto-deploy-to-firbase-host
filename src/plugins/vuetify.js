// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";
// Vuetify
import { createVuetify } from "vuetify";
export default createVuetify({
  theme: {
    // defaultTheme: 'dark',
    themes: {
      dark: {
        colors: {
          background: "#000000", // custom dark mode colors
          "red-plink": "#FF3998",
          bg_sign_up: "#000000",
          lightBg: "#BB86FC"

        },
      },
      light: {
        colors: {
          background: "#FFFFFF", //custom light mode colors
          "red-plink": "#FF3998",
          bg_sign_up: "#FFFFFF",
          lightBg: "#EEEEEE"
        },
      },
    },
  },

  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
});
