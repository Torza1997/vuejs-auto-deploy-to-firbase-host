const FooterMenu ={
    About:[
        {MenuN: 'About',Link:'#'},
        {MenuN: 'Terms',Link:'#'},
        {MenuN: 'Legal',Link:'#'},
    ],
    NFT:[
        {MenuN: 'OpenSea',Link:'#'},
        {MenuN: 'Maker',Link:'#'},
        {MenuN: 'Learn',Link:'#'},
    ],
    Contact:[
        {MenuN: 'Press',Link:'#'},
        {MenuN: 'Support',Link:'#'},
    ],
    Social  :[
        {MenuN: 'Twitter',Link:'#'},
        {MenuN: 'Terms',Link:'#'},
    ],

}
export{
    FooterMenu
}