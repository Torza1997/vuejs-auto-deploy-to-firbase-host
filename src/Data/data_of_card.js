import Nft1  from '../assets/images/Nft1.svg'
import Nft2  from '../assets/images/Nft2.svg'
import Nft2_2  from '../assets/images/Nft2_2.svg'
import Nft3  from '../assets/images/Nft3.svg'
import Nft4  from '../assets/images/Nft4.svg'
import Nft5  from '../assets/images/Nft5.svg'

import CrytoType from '../assets/images/cryto_type.svg'

const cardObject = [
    {
        id:1,
        nftImage: Nft1,
        textSeries: 'Gloop series',
        nameOfCard: 'Purple Mand',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:2,
        nftImage: Nft2,
        textSeries: 'Gloop series',
        nameOfCard: 'No name',
        priceOfEth: 3.95,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '2 day left',
        hash: "#1094",
        crytoType: CrytoType,
    }
]
const CardObject2 = [
    {
        id:1,
        nftImage: Nft2_2,
        textSeries: 'Gloop series',
        nameOfCard: 'Purple Mand',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:2,
        nftImage: Nft3,
        textSeries: 'Gloop series',
        nameOfCard: 'Beige',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:3,
        nftImage: Nft4,
        textSeries: 'Gloop series',
        nameOfCard: 'Red Mand',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },{
        id:4,
        nftImage: Nft5,
        textSeries: 'Gloop series',
        nameOfCard: 'Green',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:5,
        nftImage: Nft5,
        textSeries: 'Gloop series',
        nameOfCard: 'Green',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    }
    ,
    {
        id:6,
        nftImage: Nft5,
        textSeries: 'Gloop series',
        nameOfCard: 'Green',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:7,
        nftImage: Nft5,
        textSeries: 'Gloop series',
        nameOfCard: 'Green',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },
    {
        id:8,
        nftImage: Nft5,
        textSeries: 'Gloop series',
        nameOfCard: 'Green',
        priceOfEth: 2.99,
        cryptoType: 'eth',
        topStatus: 'top bid',
        date: '1 day left',
        hash: "#12983",
        crytoType: CrytoType,
    },

]
export {
    CardObject2,
    cardObject
} 