import IconEth1 from "../assets/images/Eth.svg";
import IconEth2 from "../assets/images/cryto_type.svg";

const DataOfCard = [
  {
    id: 1,
    Image: IconEth2,
    Header: "An NFT like no other",
    Paragraph1:
      "Don’t miss out on the release of our new NFT. Sign up below to receive updates when we go live on 18/9.",
    Paragraph2:
      "Don’t miss out on the release of our new NFT. Sign up below to receive updates when we go live on 18/9.Don’t miss out on the release of our new NFT. ",
    BgXs: "#EEEEEE",
  },
  {
    id: 2,
    Image: IconEth1,
    Header: "An NFT like no other",
    Paragraph1:
      "Don’t miss out on the release of our new NFT. Sign up below to receive updates when we go live on 18/9.",
    Paragraph2:
      "Don’t miss out on the release of our new NFT. Sign up below to receive updates when we go live on 18/9.Don’t miss out on the release of our new NFT. ",
    BgXs: "#FFFFFF",
  },
];
export { DataOfCard };
