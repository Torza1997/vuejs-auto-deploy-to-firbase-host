import brand1 from "../assets/images/brand1.svg";
import brand2 from "../assets/images/brand2.svg";
import brand3 from "../assets/images/brand3.svg";
import brand4 from "../assets/images/brand4.svg";
import brand5 from "../assets/images/brand5.svg";

const brands = [
  { id: 1, Img: brand1, BranName: "Boomerang" },
  { id: 2, Img: brand2, BranName: "Blimp" },
  { id: 3, Img: brand3, BranName: "qrco" },
  { id: 4, Img: brand4, BranName: "OpenDoor" },
  { id: 5, Img: brand5, BranName: "Droplet" },
];
export{
  brands,
};
