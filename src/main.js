import { createApp} from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import router from './router'
import menu from './Data/globlaVariable'
import $ from 'jquery'


loadFonts()
const app = createApp(App)
//set config global property
app.config.globalProperties.$Jquery = $;
app.provide('menuByprovider', menu)
//set using extend Evaronment for app 
app.use(router)
app.use(vuetify)
app.mount('#app')

// const Test = {Test:function(){
//     return 'hello test'
// }}
// Object.assign(app._component.computed,Test)
// console.log(app)

// const {xs,sm,md,lg,xl} = app.$vuetify.display
// console.log(xs,sm,md,lg,xl)
// createSSRApp(function(){console.log("log on server")})
